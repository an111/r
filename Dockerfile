# basic OS image
FROM python:3.8-slim
  
# project sources
#COPY requirements.txt requirements.txt
RUN python -m pip install --upgrade pip
RUN pip install flask redis flask_apscheduler requests python-dotenv gevent gunicorn[gevent]
RUN pip cache purge
RUN mkdir web_app && cd web_app
COPY . /web_app
WORKDIR web_app
EXPOSE 8002
ENV FLASK_APP=main.py
#ENTRYPOINT flask run --host 0.0.0.0 --port 8002 
#CMD uvicorn main:app --host 0.0.0.0 --port 8002
#CMD python3 main.py
#CMD flask run --host 0.0.0.0 --port 8002 
# SHELL ["/bin/bash", "-c"]
#ENTRYPOINT ['python3','test.py']
#CMD [ "flask" , "run" , "--host", "0.0.0.0" , "--port", "8002" ]
CMD ["gunicorn","--config","gunicorn.conf.py", "wsgi:app","-p", "app.pid", "--log-level","debug"] 
