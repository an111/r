from flask import Flask,jsonify,request,abort,Response
import json,requests
import sys
import signal
import redis
import time
from requests.exceptions import HTTPError
import os
from flask_apscheduler import APScheduler
from itertools import cycle
from os import environ as env
from dotenv import load_dotenv
load_dotenv()

#url='https://lab.karpov.courses/hardml-api/module-5/get_secret_number'
url=env['URL']
# replicas_pool=cycle([])

# def round_robin():
#     return next(replicas_pool)
os.environ['REPLICA_NAME']='replica_name'
os.environ['PORT']='8002'
os.environ['HOST']='95.217.188.148'

def init_retry(url, max_tries=40):
    for i in range(max_tries):
        try:
           time.sleep(0.3) 
           r=json.loads(requests.get(url).content.decode(encoding='utf-8'))
           #print(r)
           if r['status']!='Все ок':
                continue
           break 
        except Exception:
            continue

def discover(service_name):
    def wrapper_discover():
        replicas_pool=[]
        print(service_name)
        redis_db=redis.Redis(host='95.217.188.148',port='6379',decode_responses=True,password="Profess19")
        service_replicas:list=redis_db.lrange(service_name,0,-1)    
        for replica in service_replicas:
            host,port=redis_db.hmget(replica,keys=['host','port'])
            if (host is None) and (port is None):
                redis_db.delete(service_name,replica)
                redis_db.hdel(replica)
                replicas_pool.remove(tuple([replica,host,port]))
            elif (host is not None) and (port is not None):
                replicas_pool.append(tuple([replica,host,port]))
                pass
            else:
                raise RuntimeError
        print(replicas_pool)
        #replicas_pool=cycle(replicas_pool)
    return wrapper_discover 

def init_scheduler(service_name):
    scheduler=APScheduler()
    scheduler.add_job(id='lol', 
                    func=discover(service_name=service_name),
                    trigger='interval',
                    seconds=5
                    )
    return scheduler

def handler(sig,frame):
    print(f'YOU CALLED ME WITH {sig}')
    #redis_db=redis.Redis(host='95.217.188.148',port='6379',decode_responses=True,password="Profess19")
    #redis_db.delete(env['REPLICA_NAME'])
    #redis_db.lrem('web_app',1,env['REPLICA_NAME'])
    #redis_db.hdel(env['REPLICA_NAME'],env['HOST'])
    #redis_db.hdel(env['REPLICA_NAME'],env['PORT'])
    sys.exit(0)

def create_app():
    signal.signal(signal.SIGINT,handler)
    signal.signal(signal.SIGTERM,handler)
    signal.signal(signal.SIGHUP,handler)
    #init_retry(url)
    app= Flask(__name__)
    #redis_db=redis.Redis(host='95.217.188.148',port='6379',decode_responses=True,password="Profess19")
    #redis_db.rpush('web_app',env['REPLICA_NAME'])
    #redis_db.hset(env['REPLICA_NAME'], 'host',"95.217.188.148")
    #redis_db.hset(env['REPLICA_NAME'], 'port',env['PORT'])
    time.sleep(1)
    
    @app.route("/return_secret_number")
    
    def exec():   
        #for i in range(max_tries):
            
        while True:    
            try:
                response=requests.get(url)
                response.raise_for_status()
                jsonResponse = response.json()
                #response.raise_for_status()
                #response.json == 'application/json'
                #r=json.loads(response)
                print(jsonResponse)
                output=jsonify({'secret_number':jsonResponse['secret_number']})
                break
            except HTTPError as http_err:
                #print(http_err)
                continue
                #output=jsonify(str(http_err))
            except Exception as err:
                print(err)
                #output=jsonify(str(err))
                continue
                # try:
                #     response=requests.get(url,timeout=1)
                #     response.raise_for_status()
                
                # except HTTPError as http_err:
                #     l=jsonify({'status':'http_err'})
                # except Exception as err:
                #     l=jsonify({'status':'err'})
                # else:
                #     l=jsonify({'status':'OK'})
                #print(l)
        return output
    return app
    

app=create_app()

if __name__=='__main__':
    #удаляем все из регистра
    
    #redis_db.flushall()
    
    #Проверяет ответы от источника
    
    
    #Шедулер для хелсчека
    # scheduler=init_scheduler(service_name='web_app')
    # scheduler.init_app(app)
    # scheduler.start()
    
    # Регистрация реплики
    #
    #host_port = {"host":"95.217.188.148", "port":env['PORT']}
    #redis_db.keys()
    
    app.run(host='0.0.0.0',port='8002',debug=True)
    
